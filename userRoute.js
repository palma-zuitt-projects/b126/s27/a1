//import dependicies

const express = require("express");

const router = express.Router();

const userController = require("../controllers/user")



//Route to check for duplicate emails

router.post("/checkEmail", (req,res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send (resultFromController))
})


//Router for registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
})

//Route for login

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (
		resultFromController))
})



module.exports = router;


//FRONT END:|

//User registration form -> User submits form -> website first
//communicates with route to check for duplicate emails

//if no duplicates > communicate with /register route
//if there are any duplicates, stop transcation immediately